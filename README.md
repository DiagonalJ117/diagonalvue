# diagonalvue: My Resume Website :D
If you find an issue or bug, please submit an issue [right here](https://gitlab.com/DiagonalJ117/diagonalvue/-/issues).

Made with Vue.js and proudly hosted at Netlify ❤️

## Frameworks used
- Vue.js
- Nuxt.js
- Vuetify

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```
