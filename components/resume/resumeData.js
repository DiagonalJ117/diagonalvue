const data = {
    exps:[
        {
          activeTime:'March 2015 - October 2015',
          position: 'Front End Developer Intern',
          company: 'Grossman Beyond Marketing',
          activities: [
            'Worked on making websites for various clients using Django.',
            'Developed various static websites for clients using Wordpress or plain HTML, CSS and JavaScript along with Bootstrap for the UI.'
          ],
          color: 'red'
        },
      {
          activeTime:'October 2015 - July 2016',
          position: 'BackEnd Developer',
          company: 'Grupo Tress Internacional',
          activities: [
            'Using C# and .NET Web API, developed a SharePoint-based intranet solution to enhance communication between collaborators.',
          ],
          color: 'pink'
        },
        {
          activeTime:'October 2015 - July 2016',
          position: 'FrontEnd Developer',
          company: 'Grupo Tress Internacional',
          activities: [
            'Using Wordpress and Javascript, collaborated in the development of a web platform dedicated to ease personnel training and onboarding for the company.'
          ],
          color: 'deep-orange'
        },
        {
          activeTime:'September 2019 - November 2019',
          position: 'Fullstack .NET Core Web Developer',
          company: 'Xipe Technology',
          activities: [
            'Collaborated in the development and deployment (using Microsoft Azure) of a web platform for a U.S.-Based SMS marketing service using .NET Core MVC and Web API to connect with various external SMS and payment services.'
          ],
          color: 'green'
        },
        {
          activeTime:'June 2017 - Present',
          position: 'Fullstack Web Developer',
          company: 'Freelance',
          activities: [
            'Creating and Delivering amazing web solutions tailored to the clients needs and wants while maintaining a work-life balance.',
            'Developed an online bookstore platform for local bookseller MiLibroMx using .NET Core MVC and Microsoft Azure.',
            'Developed Local bookstore business’ MiLibroMx’s homepage using React.js and Netlify.',
            'Collaborated in the development of a Nutrition Plan Generator for a local nutriologist using Laravel and Bootstrap.',
            'Developed an internal platform using .NET Core MVC for a local Electronic Repair Shop, dedicated to keep track of all repairs in the shop.',
            'Worked on a web application for Image Projector and Electronic component loaning in Economics building at Instituto Tecnológico de Hermosillo using Django.',
            'Developed a Web Application for Computer Room access registry for Economics building in Instituto Tecnológico de Hermosillo using Django.',
            'Web Application for Cognitive Behavioral Therapy using React.js and Node.js.',
            'Developed an SOS App to aid authorities in mapping Gender-based violence cases using React Native, using Google Firebase as the backend.',
          ],
          color: 'blue'
        }
      ],
      edus:[
        {
            college: 'Universidad TecMilenio',
            major: 'Graphic Design and Animation',
            period: 'August 2013 - April 2015',
            color: 'green',
            titleColor: 'green'
        },
        {
            college: 'Instituto Tecnológico de Hermosillo',
            major: 'Engineering Informatics',
            period: 'August 2016 - Present',
            grad: 'December 2020',
            color: 'cyan',
            titleColor: 'cyan'
        }
    ],
    skills: [
        {
            title: 'Base Languages',
            elements:[
                'HTML and CSS',
                'JavaScript',
                'Python',
                'Java',
                'PHP',
                'C#'
            ]
        },
        {
            title: 'Frameworks and runtimes',
            elements: [
                '.NET Core',
                'React.js',
                'Vue.js',
                'Bootstrap',
                'Laravel',
                'Django',
                'Node.js'
            ]
        },
        {
            title: 'Tools',
            elements:[
                'Git CLI', 
                'GitLab', 
                'Postman', 
                'Gitkraken', 
                'Jira'
                ],
        },
        {
            title: 'Cloud Services',
            elements: [
                'AWS',
                'Microsoft Azure',
                'Google Firebase',
                'DigitalOcean',
                'Netlify'
            ]
        },


    ],
    promSkills: [
        {
            title: 'Base Languages',
            elements:[
                'JavaScript',
                'Python',
                'C#'
            ]
        },
        {
            title: 'Frameworks and runtimes',
            elements: [
                '.NET Core',
                'React.js',
                'Vue.js',
            ]
        },
        {
            title: 'Tools',
            elements:[
                'Git',
                'Postman', 
                'Jira'
                ],
        },
        {
            title: 'Cloud Services',
            elements: [
                'AWS',
                'Microsoft Azure',
                'Netlify'
            ]
        },
    ],

}

const jsonData = data
export default jsonData;